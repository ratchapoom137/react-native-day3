import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Screen1 from './Screen1'
import Screen2 from './Screen2'
import Login from './Login'
import Profile from './Profile'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route path="/screen1" component={Screen1} />
                    <Route path="/screen2" component={Screen2} />
                    <Route path="/login" component={Login} />
                    <Route path="/profile" component={Profile} />
                    <Redirect to="/screen1" />
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router