/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableOpacity, TextInput, Image, Modal, TouchableHighlight } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

export default class App extends Component {

    state = {
        visible: false,
    };

    // UNSAFE_componentWillMount() {
    //     console.log(this.props)
    // }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.visible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        this.handleCancel;
                    }}
                >
                    <TouchableOpacity style={styles.modal} onPress={() => { this.handleCancel(); }}>
                        <Text style={styles.textModal}>Close</Text>
                    </TouchableOpacity>
                </Modal>
                <View style={styles.header}>
                    <View style={styles.box1}>
                        <Text style={styles.text}> back </Text>
                    </View>
                    <View style={styles.box2}>
                        <Text style={styles.text}>My Profile</Text>
                    </View>
                </View>
                <View style={styles.content}>
                    <TouchableOpacity style={styles.boxProfile} onPress={() => { this.showModal(); }}>
                        <Text style={styles.textTitle}>Username</Text>
                        <Text>{this.props.location.state.Username}</Text>
                        <Text style={styles.textTitle}>First name</Text>
                        <Text>{this.props.location.state.Firstname}</Text>
                        <Text style={styles.textTitle}>Last name</Text>
                        <Text>{this.props.location.state.Lastname}</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: 'white',
    },
    header: {
        backgroundColor: "white",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: -8
    },

    headerText: {
        color: "black",
        fontSize: 30,
        fontWeight: "bold",
    },

    content: {
        backgroundColor: "white",
        flexDirection: "row",
        flex: 1
        // alignItems: "center",
        // justifyContent: "center",
    },

    image: {
        width: 150,
        height: 150,
        borderRadius: 40,
        margin: 5
    },

    box1: {
        backgroundColor: "green",
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    box2: {
        backgroundColor: "green",
        padding: 10,
        
        flex: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    
    boxProfile: {
        margin: 10,
    },

    modal: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textModal: {
        color: 'white',
        fontSize: 20
    },

    textTitle: {
        color: 'black',
        fontSize: 25,
        fontWeight: "bold",
    }

});
