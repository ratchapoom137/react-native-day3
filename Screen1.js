import React, { Component } from 'react'
import { View, Text, Alert } from 'react-native'
import { Link } from 'react-router-native'

class Screen1 extends Component {
    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.mynumber) {
            Alert.alert('My number', this.props.location.state.mynumber + '')
        }
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: 'blue', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{color: 'black', fontSize: 30, margin: 10}}>Screen1</Text>
                <Link to='/screen2'>
                    <Text style={{color: 'white', fontSize: 30, margin: 10}}>Go to screen2</Text>
                </Link>
                <Link to='/login'>
                    <Text style={{color: 'white', fontSize: 30, margin: 10}}>Go to Login</Text>
                </Link>
            </View>
        )
    }
}

export default Screen1