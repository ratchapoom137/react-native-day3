/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableOpacity, TextInput, Image, Modal, Icon } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

export default class App extends Component {

    state = {
        visible: false,
        username: '',
        password: '',
        firstname: '',
        lastname: '',
        status: '',
    };

    // constructor(props) {
    //   super(props);
    //   this.state = { username: '' };ss
    // }

    onClickLogin = () => {
        const Username = this.state.username
        const Password = this.state.password
        // const Firstname = this.state.firstname
        // const Lastname = this.state.lastname
        this.props.history.push('/profile', {
            Username
            // , Password, Firstname, Lastname
        })
    }

    handleCancel = (e) => {
        console.log(e);
        this.setState({ visible: false })
    }

    render() {
        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.visible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        this.handleCancel();
                    }}
                >
                    <TouchableOpacity style={styles.modal} onPress={() => { this.handleCancel(); }}>
                        <Text style={styles.textModal}>{this.state.status}</Text>
                    </TouchableOpacity>
                </Modal>
                <View style={styles.header}>
                    <View style={styles.image}>
                        <Image
                            style={{ width: 200, height: 200, borderRadius: 100 }}
                            source={{ uri: 'https://sv1.picz.in.th/images/2019/02/12/ToPrc9.jpg' }}
                        />
                    </View>
                </View>
                <View style={styles.content}>
                    <View style={{ alignItems: 'center' }}>
                        <Text >{this.state.username} - {this.state.password}</Text>
                    </View>
                    <View style={styles.box1}>
                        <TextInput style={styles.text} placeholder="Username" onChangeText={value => this.setState({ username: value })} />
                    </View>
                    <View style={styles.box1}>
                        <TextInput style={styles.text} tyle="password" placeholder="Password" onChangeText={value => this.setState({ password: value })} />
                    </View>
                    {/* <View style={styles.box1}>
                        <TextInput style={styles.text} placeholder="First name" onChangeText={value => this.setState({ firstname: value })} />
                    </View>
                    <View style={styles.box1}>
                        <TextInput style={styles.text} placeholder="Last name" onChangeText={value => this.setState({ lastname: value })} />
                    </View> */}
                    <View style={styles.box2}>
                        <Button title="Login" onPress={() => { this.onClickLogin(); }} />
                    </View>
                </View>
                {/* <View style={styles.content}> */}
                    
                {/* </View> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#66ccff',
    },
    header: {
        backgroundColor: "#66ccff",
        alignItems: "center",
        justifyContent: "center",
        // width: 200,
        // height: 200,
        // borderRadius: 100
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    image: {
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: 'center',
        width: 200,
        height: 200,
        borderRadius: 100
    },
    headerText: {
        color: "black",
        fontSize: 30,
        fontWeight: "bold",
    },

    content: {
        backgroundColor: "#66ccff",
        flex: 1,
        flexDirection: "column",
    },

    box1: {
        backgroundColor: "white",
        margin: 3,
    },

    box2: {
        backgroundColor: "black",
        margin: 50,
    },

    row: {
        backgroundColor: "#66ccff",
        padding: 5,
        margin: 8,
        flexDirection: "row",
        // alignItems: 'center',
        // justifyContent: 'center'
    },

    modal: {
        position: 'absolute',
        top: 220,
        right: 20,
        left: 20,
        bottom: 220,
        backgroundColor: 'rgba(0,0,0,0.8)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textModal: {
        color: 'white',
        fontSize: 20
    },

    ButtonX: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: 50
    }
});
